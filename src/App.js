import { useState } from "react"
import './App.css';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
function App() {
  const [count, setCount] = useState(0);
  const [animal, setAnimal] = useState("Click Button !!!");

  const buttonStyle = {
    width: "300px",
    margin: "auto",
  }

  return (
    <div className="App">
      <header className="App-header">
        <div className="container">
          <div className="count mb-5">
            <h4>Mari berhitung</h4>
            <h1>{count}</h1>
            <button className="btn btn-danger btn-md ms-4" onClick={() => setCount(count - 1)}>Kurang</button>
            <button className="btn btn-success btn-md ms-4" onClick={() => setCount(count + 1)}>Tambah</button>
            <div className="row mt-3">
            <button className="btn btn-warning btn-md" style={buttonStyle} onClick={() => setCount(0)}>Reset</button>
            </div>
          </div>
          <div className="mt-5">
            <h2 className="text-center mb-3">Siapa aku?</h2>
            <h4 className="justify-content-center">{animal}</h4>
              <button className="btn btn-outline-info btn-md ms-3" onClick={() => setAnimal("Kucing")}>Siapa ya</button>
              <button className="btn btn-outline-info btn-md ms-3" onClick={() => setAnimal("Ikan")}>Siapa ya</button>
              <button className="btn btn-outline-info btn-md ms-3" onClick={() => setAnimal("Sapi")}>Siapa ya</button>
              <button className="btn btn-outline-info btn-md ms-3" onClick={() => setAnimal("Kambing")}>Siapa ya</button>
            <div className="row mt-3">
              <button className="btn btn-outline-warning btn-ms" style={buttonStyle} onClick={() => setAnimal("Coba Tebak")}>Reset</button>
            </div>
          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
